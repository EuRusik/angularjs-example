﻿class LotActionTrackingController {
    constructor(mgDAL, mgModule, DTOptionsBuilder, $location, mgCustomerAppService) {
        /**
         * init services
         */
        this._mgDAL = mgDAL;
        this._DTOptionsBuilder = DTOptionsBuilder;
        this._mgModule = mgModule;
        this._location = $location;
        this._mgCustomerAppService = mgCustomerAppService;
        /**
         * init vm
         */
        this.subjectName = null;
        this.bblName = null;
        this.lotActionTrackingPathHowToUse = '';
        /**
         * Datepicker
         * @type {string[]}
         */
        this.formats = ['dd.MM.yyyy', 'MM.dd.yyyy', 'dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
        this.format = this.formats[1];
        this.fromDateModel = new Date();
        this.fromDateModel.setHours(0, 0, 0, 0);
        this.fromDateModel.setDate(this.fromDateModel.getDate() - 1);
        this.toDateModel = new Date();
        this.toDateModel.setHours(0, 0, 0, 0);
        this.toDateModel.setDate(this.toDateModel.getDate() + 1);
        this.statusDatepicker = {
            fromDate: false,
            toDate: false,
        };

        /**
         * Datatables
         */
        this.dtOptions = this._DTOptionsBuilder
            .newOptions()
            .withBootstrap()
            .withPaginationType('full_numbers')
            .withDisplayLength(25);

        /**
         * init const
         */
        this.serviceConstName = 'CS_AGREEMENT_SERVICE_TYPE_TAX';

        /**
         * Filters
         */
        this.dropdownMultiselectSettings = {
            idProp: 'id',
            displayProp: 'displayName',
            scrollableHeight: '300px',
            enableSearch: true,
            externalIdProp : '',
        };

        this.filtersItems = {
            dropdownMultiselectActionNameOptions: [],
            dropdownMultiselectObjectNameOptions: [],
        };

        this.filtersSelectedItems = {
            dropdownMultiselectActionNameModel: [],
            dropdownMultiselectObjectNameModel: [],
        };

        this.dropdownMultiselectEvents = {
            dropdownMultiselectActionNameEventsFilter: null,
            dropdownMultiselectObjectNameEventsFilter: null,
        };

        this.filtersTranslations = {
            dropdownMultiselectActionNameTranslation: {
                buttonDefaultText: 'Action',
            },
            dropdownMultiselectObjectNameTranslation: {
                buttonDefaultText: 'Section or Status',
            },
        };

        this.init();
    }

    init() {
        this.constDicti = this._mgModule.getCommonDicti();
        let queryParams = this._location.search();
        if (queryParams && queryParams.borough && queryParams.block && queryParams.lot) {
            const startPeriod = moment();
            const endPeriod = moment().subtract(1, 'months');
            /**
             * refresh models
             */
            this.toDateModel = moment().format('MM.DD.YYYY');
            this.fromDateModel = moment().subtract(1, 'months').format('MM.DD.YYYY');

            const boroughName = this._mgCustomerAppService.getBoroughName(queryParams.borough);
            const bblByParam = this.bblName = `${boroughName}/${queryParams.block}/${queryParams.lot}`;
            this.getLotActionData(endPeriod, startPeriod, null, bblByParam);
        }
    }

    /**
     * get Array for table
     * @param fromDate {date}
     * @param toDate {date}
     * @param ownerSubjectNameLike {string}
     * @param bblStrLike {string}
     */
    getLotActionData(startPeriod = null, endPeriod = null, ownerSubjectNameLike = null, bblStrLike = null) {
        this.lotActionDataLoading = true;
        this._mgDAL.getLotActionTracking(startPeriod, endPeriod, this.serviceConstName, ownerSubjectNameLike, bblStrLike)
            .then((response) => {
                if (response && response.status === 200) {
                    this.lotActionDataLoading = false;
                    this.lotActionData = response.data;
                    this.defaultLotActionData = angular.copy(response.data);
                    this.preparingItemsData();
                }
            });
    }

    /**
     * Preparing options data for directive
     */
    preparingItemsData() {
        for (let property in this.filtersItems) {
            this.filtersItems[property] = [];
        }
        for (let property in this.filtersItems) {
            if (String(property) === 'dropdownMultiselectActionNameOptions') {
                this.filtersItems[property] = this.setArrayForFilterItems('actionName');
            }
            if (String(property) === 'dropdownMultiselectObjectNameOptions') {
                this.filtersItems[property] = this.setArrayForFilterItems('objectName');
            }
        }
    }

    /**
     * Creating an Array for a FilterItems
     * @param property {string} object property
     */
    setArrayForFilterItems(property) {
        const rlt = Lazy(this.defaultLotActionData).map((options) => {
            return {
                id: String(options[property]),
                displayName: (() => {
                    if (options[property] === null || typeof options[property] === 'undefined') {
                        return 'Empty';
                    }
                    if (options[property] instanceof Date) {
                        return String($filter('date')(options[property], 'MM/dd/yyyy h:mm a'));
                    }
                    return String(options[property]);
                })(),
                srcPropertyName: property,
            };
        }).toArray();
        return this.getUniqFilterItems(rlt, 'id');
    }

    /**
     * Uniq Array
     * @param field {String} id
     * @param incomeArray {array}
     */
    getUniqFilterItems(incomeArray, field) {
        return Lazy(incomeArray).uniq((object) => {
            return object[field];
        }).toArray();
    }

    /**
     * Filter the data for those models that we have not empty
     */
    applyFilter() {
        // Array for keep save filter date for any one column
        let tempArrayForOneColumn;
        // total filter data
        let tempFilterArray = angular.copy(this.defaultLotActionData);
        for (let columnModel in this.filtersSelectedItems) {
            if (this.filtersSelectedItems[columnModel].length > 0) {
                // clear array of filter data for current Column
                tempArrayForOneColumn = [];
                if (tempFilterArray && tempFilterArray.length !== 0) {
                    Lazy(this.filtersSelectedItems[columnModel]).each((coumnSelectedItem) => {
                        Lazy(tempFilterArray).each((taxAppData) => {
                            if (coumnSelectedItem) {
                                if ((taxAppData[coumnSelectedItem.srcPropertyName] !== null || typeof taxAppData[coumnSelectedItem.srcPropertyName] !== 'undefined')
                                    && String(taxAppData[coumnSelectedItem.srcPropertyName]) === coumnSelectedItem.id) {
                                    tempArrayForOneColumn.push(taxAppData);
                                }
                            }
                        });
                    });
                    tempFilterArray = angular.copy(tempArrayForOneColumn);
                }
            }
        }
        this.closeFilters();
        this.lotActionData = tempFilterArray;
    }

    /**
     * Reset all filters
     */
    clearFilters() {
        for (const ItemForEventsFilter in this.dropdownMultiselectEvents) {
            this.dropdownMultiselectEvents[ItemForEventsFilter].deselectAll();
            this.dropdownMultiselectEvents[ItemForEventsFilter].resetActionStatuses();
        }
    }

    /**
     * Close all filters
     */
    closeFilters() {
        for (const ItemForCloseFilter in this.dropdownMultiselectEvents) {
            this.dropdownMultiselectEvents[ItemForCloseFilter].closeFilter();
        }
    }

    /**
     * Open Subject Owner
     */
    openSubjectProperties(toSubjectID) {
        var url = '/property-list/' + String(toSubjectID);
        window.open(url, '_blank');
    }

    /**
     * datepiker open status
     * @param $event
     * @param property {string}
     */
    open($event, property) {
        this.statusDatepicker[property] = true;
    }

    static Create(){
        return new LotActionTrackingController(...arguments);
    }
}

(function () {
    angular.module('mgCustomerApp').controller('LotActionTrackingController', LotActionTrackingController.Create);
    LotActionTrackingController.Create.$inject = ['mgDAL', 'mgModule', 'DTOptionsBuilder', '$location', 'mgCustomerAppService'];
})();